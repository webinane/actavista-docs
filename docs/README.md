---
home: false


---

**Note:** *In case of any problem regarding Actavista, we recommend to join our forum at Support Forum or email us at <a href="mailto:support@webinane.com">support@webinane.com</a>*

Thanks for your patience and appreciation—the premium WordPress version of the Actavista theme is finally out! It comes with super-responsive layout, extreme flexibility, advanced SEO coding and neat & clean design, which are the most demanded features for such a magnificent product devoted to the holy/virtuous cause. Expressive mouse-hover animations and super-refreshing retina ready graphics are pleasing to the eye and soothing to the heart.

With Actavista premium WordPress theme you can not only launch a perfect and progressing Political, Charity and NGO website, but also save a large sum of $ 63 on account of the free-of-cost provision of Revolution Slider (worth $ 18) and Visual Composer (worth $ 28) and Layer Slider (worth $ 17).

The modern page builder WP plugin makes the process of page building and customization as easy as a child’s play.The 6 brilliant Homepage layouts, 5 innovative header styles, 15 color schemes, and plenty of built-in widgets, shortcodes & PSD files promote the versatile utilization of the product.

Additional appeal is rendered to Actavista through multi-language support, WooCommerce plugin, cross browser compatibility, and an unlimited number of Google Fonts Families and Font Awesome Icons.

## Our Motto
The sole determination of our highly skilled and devoted team workers is to bring innovation in the field of template development that will provide the users with something that they would have never experienced before. 

While the website designing is becoming a promising business, there are also many serious and complex issues that are being faced by the global web community. The same, otherwise disappointing, problems are being addressed here for the utmost facilitation and convenience of the clients.

## The Package Includes
- Best for Political and NonProfit websites
- WooCommerce Ready
- Sharp Retina Display
- Fifteen (15) eye-catching pre-defined color schemes
- Six (6) unique Homepage layouts and 5 catchy Header styles
- 1170 pixels width
- Modern Visual Composer page builder WP plugin (save $ 28)
- Revolution Slider included (Save $ 18)
- Layer Slider included (Save $ 17)
- Advanced SEO Coding
- Highly responsive layout
- Easy customization with Drag & Drop facility
- Super flexibility
- Cross browser compatibility
- Built-in Social Media buttons
- Full and boxed layout
- Three Gallery pages (2 columns, 3 columns and 4 columns)
- Six (6) extra Sidebar widgets
- Flicker widget for the Footer
- Over 650 Google Fonts Families and 350+ Font Awesome Icons
- Four (4) patterns for boxed layout
- Two (2) unique “Donate Us” box designs
- Separate Pages for “Successful Stories” and “Projects”
- Four (4) Single Post styles (with Project, Video, Featured Image and Slider)
- Event Management pages included
- Support for Contact Form 7
- Sliding hover effect on menu
- Usage of light JQuery Code
- 6 PSD files included in the package
- Complete and detailed Documentation



##  Required plugins for Actavista
- Actavista Custom Plugin
- Slider Revolution
- WPBakery Page Builder
- Webinane Donation
- WooCommerce WP
- Webinane External Resource


