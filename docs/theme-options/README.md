---


---
# Actavista Options

## General Setting
- `Color Scheme` - You can select your theme's color here. by **Apperance** => **Actavista Options** => **General Settings** This color will be the main color.

![Alt Text](./assets/images/new-1.png)

### APIs Settings

![Alt Text](./assets/images/api1.png)
- `Twitter APIs Configuration Section` - These fields must be filled because if any of the field remains empty then no twitter data will be shown on twitter widget.

![Alt Text](./assets/images/api5.png)

- `Google Map` - Enter the google map api key for your site. With the help of this api key, we you can easily show google map on the site.

![Alt Text](./assets/images/api2.png)

## Header Setting
 - `Choose Header` Select the custom header as per your requirement.
 
 ![Alt Text](./assets/images/header-1.png)

### Header One Setting
- `Header One Setting` If you use header One style, just add your own logos and site banners etc..

 ![Alt Text](./assets/images/header-2.png)
### Header Two Setting
- `Header Two Setting`  If you use header Two style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/header-3.png)
 ### Header Three Setting
 - `Header Three Setting`  If you use header Three style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/header-4.png)
 ### Header Four Setting
- `Header Four Setting`  If you use header Four style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/header-5.png)
 ### Header Five Setting
 - `Header Five Setting`  If you use header Five style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/header-6.png)

 ### Header Six Setting
 - `Header Six Setting`  If you use header Six style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/header-7.png)

### Header Seven Setting
 - `Header Seven Setting`  If you use header Seven style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/header-8.png)

### Responsive Header Setting
 - `Responsive Header Setting`  If you use responsive header style, just add your own logos and site banners etc.

 ![Alt Text](./assets/images/header-9.png)

 ### Main Menu Typography
 - `Main Menu Typography` - This setting options will customize header style one and header style two Main Menu.

 ![Alt Text](./assets/images/header-10.png)
 - Enable to show menu bar typography for header both header styles.

## Footer
### Footer Settings
- `Style` - Select the custom footer as per your requirement.

![Alt Text](./assets/images/footer-1.jpeg)

### Footer Style One
- `Footer Style One` Select footer background color &  footer background.

![Alt Text](./assets/images/footer-2.jpeg)

### Footer Style Two
- `Footer Style Two` If you use Footer style Two, just add your own logos and background Colors etc.

![Alt Text](./assets/images/footer-3.jpeg)

### Footer Style Three
- `Footer Style Three` If you use Footer style Three, just add your own logos and background Colors and Select numbers of Social Icons.

![Alt Text](./assets/images/footer-4.jpeg)

## Blog Setting

### Blog Page Settings

- `Blog Page Settings` You can set blog layouts by just clicking `ON` or `OFF` buttons below given the options of any blog sections or can change the text & images. 

![Alt Text](./assets/images/blog.jpeg)

### 404 Page Settings

- `404 Page Settings` You can set 404 page layout by just clicking `ON` or `OFF` buttons below given the options of any 404 page sections or can change the text & images. 

![Alt Text](./assets/images/404-page.jpeg)

### Author Page Settings

- `Author Page Settings` You can set Author page layout by just clicking `ON` or `OFF` buttons below given the options of any Author page sections or can change the text & images. 

![Alt Text](./assets/images/author-page.jpeg)

### Archive Page Settings

- `Archive Page Settings` You can set Archive page layout by just clicking `ON` or `OFF` buttons below given the options of any Archive page sections or can change the text & images. 

![Alt Text](./assets/images/archive-page.jpeg)

### Category Page Settings
- `Category Page Settings` You can set Category page layout by just clicking `ON` or `OFF` buttons below given the options of any Category page sections or can change the text & images. 

![Alt Text](./assets/images/category-page.jpeg)
 
### Tag Page Settings
- `Tag Page Settings` You can set Tag page layout by just clicking `ON` or `OFF` buttons below given the options of any Tag page sections or can change the text & images. 

![Alt Text](./assets/images/tag-page.jpeg)

### Search Page Settings
- `Search Page Settings` You can set Search page layout by just clicking `ON` or `OFF` buttons below given the options of any Search page sections or can change the text & images. 

![Alt Text](./assets/images/search-page.jpeg)

### Single Post Settings
- `Single Post Settings` You can set Single post layout by just clicking `ON` or `OFF` buttons below given the options.

![Alt Text](./assets/images/single-post-page.jpeg)

## Templates

### Event Settings
- `Event Settings` You can set Event layout by just clicking `ON` or `OFF` buttons below given the options.

![Alt Text](./assets/images/event-settings.jpeg)

### Video Settings
- `Video Settings` You can set Video layout by just clicking `ON` or `OFF` buttons below given the options.

![Alt Text](./assets/images/video-settings.jpeg)

### Coming Soon Settings
- `Coming Soon Settings` You can set the Comming soon page layout by just clicking `ON` or `OFF` buttons below given the options.

![Alt Text](./assets/images/coming-soon.jpeg)

## Language Settings
- First Select your langue and then upload .mo language file.
![Alt Text](./assets/images/languge.jpeg)

## Custom Font Settings
- Please upload your desire font file in *.ttf, *.otf, *.eot, *.woff format.
![Alt Text](./assets/images/custom-font.jpeg)

## Typography Settings
These are the options for any change in heading's typography. If you are not satisfied with default heading typography then you can easily change you desired typo.

![Alt Text](./assets/images/ht.png)

### Body Typography Settings
Enable to customize the theme Apply options to customize the body,paragraph fonts for the theme.

![Alt Text](./assets/images/bt.png)

## Import/Export

### Import Options
Suppose if have two websites and want same theme options then you should go on old website  and download or export theme options with exporter. Now go to new website importer and upload the file. After uploading you will see both websites have same theme options. 

### Export Options
Here you can copy/download your current option settings. Keep this safe as you can use it as a backup should anything go wrong, or you can use it to restore your settings on this site (or any other site).

![Alt Text](./assets/images/ie.png)
