---


---


# Post Types
Common Settings:- Here you will see common post meta settings. All post types have same meta.
**Header Settings:**
- `Header Style` - You will see only one option here which is "Header style". If you want to set every page with different headers the you should this option.


**Title Settings:**
- `Show Title Section` - Enable to show title banner section on this page.
- `Header Banner Custom Title` - Enter the custom title for header banner section
- `Show Breadcrumb` - Click on check box if you want to show breadcrumb.
- `Title Section Background` - Upload background image for page title section.

**Sidebar Layout:**
- `Sidebar Layout` - You can se main content and sidebar alignment with this option.




## Causes
- - Go to"**Causes**" => "**Add New**".
- Add your title and explaination to the given places.

![Alt Text](../assets/images/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/ns9.png)
- You can add **category** from the given section.

![Alt Text](./assets/images/cc.png)
- You can also insert the **featured image** of your service.

![Alt Text](../assets/images/ns11.png)
- Select the format of the cause post.
- Enter your desire amount in USD currency.

![Alt Text](./assets/images/cf.png)
- Upload gallery images.
- Enter location of the project
- Enter cause link like &quot;https://www.youtube.com/watch?v=IvWjhp62zhM&quot;

![Alt Text](./assets/images/gi.png)

## Projects
- Go to"Projects" => "Add New".
Add your title and explaination to the given places.

![Alt Text](../assets/images/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/ns9.png)
- Enter the donation amount in USD currency.

![Alt Text](./assets/images/dn.png)
- Enter geo location of the project.

![Alt Text](./assets/images/pl.png)
- You can add **category** from the given section.

![Alt Text](./assets/images/pc.png)
- You can also insert the **featured image** of your service.

![Alt Text](../assets/images/ns11.png)
## Volunteer Mangement 
 - **Create Volunteer Opportunities**
 
 - Go to"**Volunteer Mangement**" => "**Add New Opportunities**"

![Alt Text](../assets/images/voln1.png)

- Add your title and Description for Volunteer Opportunities.

![Alt Text](../assets/images/voln2.png)

- You can change your page banner or can make other layout related changes.

![Alt Text](../assets/images/voln3.png)

- Add your Volunteer Contact information, Address information and select your Event time and date. 
- Also have got the option to add limited Volunteers. 

![Alt Text](../assets/images/voln4.png)

- Once the volunteers will submit the volunteer form. All volunteers detials will show up here!

![Alt Text](../assets/images/voln5.jpeg)

- Send Emails to your Volunteers strate from your dashboard. 
## Events
- Go to"**Events**" => "**Add New**".
- Add your title and explaination to the given places.

![Alt Text](../assets/images/ns1.png) 

- Add Your Event Details with Start & End Date.
- Select Your Static block from dropdown list it will showup on your event detail page.

![Alt Text](../assets/images/event-1.jpeg)

## Videos

- Go to"Vidoe" => "Add New".
Add your title and explaination to the given places.

![Alt Text](../assets/images/ns1.png)

- Add your Video link.

![Alt Text](../assets/images/video-1.jpeg)

- Can manage your vidoe page layout from page meta options.

![Alt Text](../assets/images/video-2.jpeg)

 
## Gallery
- Go to"**Gallery**" => "**Add New**".
- Add your **title** in the this field.

![Alt Text](../assets/images/g1.png)
- Click on **Add/Edit Media** button to choose images for your gallery.

![Alt Text](../assets/images/g2.png)
- Click on **Show Video Grabber** button. Once you click on it. A pop up will show on same place.

Enter the URL of VIMEO / DailyMotion / YouTube videos to upload, you can upload multiple videos at once with comma separated with new line.
In last, click on Proceed videos.

![Alt Text](../assets/images/g3.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](../assets/images/ns9.png)
- You can add **category** from the given section.

![Alt Text](../assets/images/g10.png)
- You can also insert the **featured image** of your team member.

![Alt Text](../assets/images/ns11.png)