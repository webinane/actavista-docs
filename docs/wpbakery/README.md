---


---

# VC Shortcodes 

After login, Go to `Dashbord`. Hover on `New` button which is in top bar. And select any post type in which you want to creat your post.

![Alt Text](../assets/images/vc4.png)
Now Click on `Backend Editor` and then click on `Plus icon`. When will you tap a pop will show on your screen.

![Alt Text](../assets/images/vc1.png)
In this popup you will see many shortcodes. Click on `Webinane` to see our shortcodes for lifeline2. 

![Alt Text](../assets/images/vc2.png)

Whenever you want to use VC shortcode then you should follow these steps which we have expained above.

`Common Settings:-` Here you will see all common settings which you will saw again and again in many shortcodes. That's why we decided to put all common things at one place.

### Slider Settings
**Note:** This settings apply on some styles of shortcodes. It is not for all.
- `Apply Slider` - Enable this button to apply slider on shortcode.
- `Slides To Show` - How many slides do you want to show?
- `Slides To Scroll` - How many slides should be move on scroll?
![Alt Text](./assets/images/vc85.png)

- `Dots` - If you need dot navigator for your slider then click on check box.
- `Arrows` - If you need arrow navigator for your slider then click on check box.
- `Repeat` - Slider will repeat his items but work with click.
- `Fade` - Fade is an animation which you can apply on your slider.
- `CSS Ease` - It is the type of animation.

 ![Alt Text](./assets/images/vc7.png)

- `Stop On Hover` - Slider will stop when mouse hover on it.
- `Slide Speed` - Select the speed of slider.
- `Autoplay` - Click on check box to run autoplay.
- `Autoplay Speed` - In how much time, the Slider will repeat his items? Just add any number (mili seconds) at given place.
- `Mouse Drag` - Enable mouse drag option with this button.

 ![Alt Text](./assets/images/vc8.png)

- `RTL Direction` - Check the box if you wany to use this shorcode in RTL (Right To Left).
- `Responsive Devices` - You can set all responsive setting from here.

 ![Alt Text](./assets/images/vc86.png)

### Responsive Slider Setting
**Note:** This settings apply on some styles of shortcodes. It is not for all.

- `Apply Slider On Responsive` - Enable this button to apply responsive slider on shortcode.
- `Autoplay Speed` - In how much time, the Slider will repeat his items? Just add any number (mili seconds) at given place. 
- `Slide Speed` - Choose the speed of slider.

 ![Alt Text](./assets/images/vc87.png)

- `RTL Direction` - Check the box if you wany to use this shorcode in RTL (Right To Left).
- `Responsive Devices` - You can set all responsive setting from here.

 ![Alt Text](./assets/images/vc86.png)

## Project Listing

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_projects_listing.jpg)


### General

- `Enable Gap` - Enable this button if you want space between project listing.
- `Grid View Listing Style` - Choose projects grid view columns to show in this section.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

 ![Alt Text](./assets/images/vc5.png)
- `Number` - Enter the number of projects to show
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Donation Info` - Enable this button to show donation infomation for any project.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place. 

 ![Alt Text](./assets/images/vc6.png)


### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.

## Events

### Style 1 Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_events.jpg)

- `Styles` - Select the style number one. Once you will click on style, settings will be changed according to the style.
- `Overlap` - Click on the button to overlap your event.
- `One Post` - If you want to show any specific event then turn on this button and choose the event.

![Alt Text](./assets/images/vc89.png)

- `Number` - Enter the number of projects to show
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Enable Date` - Enable to show publish date.

 ![Alt Text](./assets/images/vc9.png)

- `Enable Counter` - Enable to show event counter. Note: Counter will only show if you have selected event post type in query.
- `Counter Days Label` - Enter counter "Days" label to show. Type any name here if you want to set another name insted "Days".
- `Counter Hours Label` - Enter counter "Hours" label to show. Type any name here if you want to set another name insted "Hours".
- `Counter Minutes Label` - Enter counter "Minutes" label to show. Type any name here if you want to set another name insted "Minutes".
- `Counter Seconds Label` - Enter counter "Seconds" label to show. Type any name here if you want to set another name insted "Seconds".

 ![Alt Text](./assets/images/vc11.png)

### Style 2 Preview

![Alt Text](./assets/images/vc88.png)

- `Styles` - Select the style number two. Once you will click on style, settings will be changed according to the style.
- `One Post` - If you want to show any specific event then turn on this button and choose the event.
- `Number` - Enter the number of projects to show

![Alt Text](./assets/images/vc90.png)

- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Title Limit` Enter the limit numbers of the title words.

![Alt Text](./assets/images/vc91.png)

- `Text Limit` Enter text words limit to show in this section.
- `Enable Button` Enable to show this button.
- `Button Label` Enter button label that you wants to show.

![Alt Text](./assets/images/vc92.png)

- `Enable Counter` - Enable to show event counter. Note: Counter will only show if you have selected event post type in query.
- `Counter Days Label` - Enter counter "Days" label to show. Type any name here if you want to set another name insted "Days".
- `Counter Hours Label` - Enter counter "Hours" label to show. Type any name here if you want to set another name insted "Hours".
- `Counter Minutes Label` - Enter counter "Minutes" label to show. Type any name here if you want to set another name insted "Minutes".
- `Counter Seconds Label` - Enter counter "Seconds" label to show. Type any name here if you want to set another name insted "Seconds".

 ![Alt Text](./assets/images/vc11.png)

### Style 3 Preview

![Alt Text](./assets/images/vc93.png)

- `Styles` - Select the style number three. Once you will click on style, settings will be changed according to the style.
- `One Post` - If you want to show any specific event then turn on this button and choose the event.
- `Grid View Style` - This option not apply, when one post is enabled.
- `Number` - Enter the number of projects to show

![Alt Text](./assets/images/vc94.png)

- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Title Limit` Enter the limit numbers of the title words.

![Alt Text](./assets/images/vc95.png)

- `Enable Counter` - Enable to show event counter. Note: Counter will only show if you have selected event post type in query.
- `Counter Days Label` - Enter counter "Days" label to show. Type any name here if you want to set another name insted "Days".
- `Counter Hours Label` - Enter counter "Hours" label to show. Type any name here if you want to set another name insted "Hours".
- `Counter Minutes Label` - Enter counter "Minutes" label to show. Type any name here if you want to set another name insted "Minutes".
- `Counter Seconds Label` - Enter counter "Seconds" label to show. Type any name here if you want to set another name insted "Seconds".

 ![Alt Text](./assets/images/vc11.png)


## Stories

### Style 1 Preview
![Alt Text](./assets/images/vc100.png)


### General

- `Style` - Select the style number one. Once you will click on style, settings will be changed according to the style. 
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` Enter the limit numbers of the title words.

![Alt Text](./assets/images/vc96.png)

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.


### Style 2 Preview
![Alt Text](./assets/images/vc101.png)


### General

- `Style` - Select the style number two. Once you will click on style, settings will be changed according to the style. 
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` Enter the limit numbers of the title words.

![Alt Text](./assets/images/vc97.png)

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.


### Style 3 Preview
![Alt Text](./assets/images/vc102.png)


### General

- `Style` - Select the style number one. Once you will click on style, settings will be changed according to the style. 
- `Type` - Select the type of the story i.e Single ot Multiple.
- `Select Story` - This option is only available when you choose type "single". Here you can select any specific story.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

![Alt Text](./assets/images/vc98.png)

- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` Enter the limit numbers of the title words.
- `Button Label` Enter button label that you wants to show.

![Alt Text](./assets/images/vc99.png)


### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.


## Heading


- `Heading Style` - Choose one heading style to show in this shortcode view.
- `Heading Color` - Choose the color for your selected heading style.
- `Heading Align` - You can set the alignment of your selected header style.

 ![Alt Text](./assets/images/vc14.png)

- `Title` - %STR_COLOR% used to start the theme color and %ED_COLOR% used to close the theme color.

- `Subtitle` - Write subtitle at given place to show up or below the heading.


 ![Alt Text](./assets/images/vc15.png)

## Our Clients

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_clients.jpg)


### General

- `Style`  - Choose one style for this shortcode view.
- `Add Clients`  - Click on `plus sign` to add more fields for clients.
 ![Alt Text](./assets/images/vc16.png)

- `Client Image`- Click to upload the image of client.
- `Custom Link`-  Enter URL of client website if you wants to link.

 ![Alt Text](./assets/images/vc17.png)



### Slider Settings

[Click here](/wpbakery/#slider-settings) to see slider setting.

## Testimonial

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_testimonial.jpg)


### General

- `Grid View` - With this option you can apply slider on testimonial or not. If you need slider then select **Slider** otherwise select **Grid**.
- `Testimonial List` - Click on plus icon to add more testimonials in list.


 ![Alt Text](./assets/images/vc23.png)
- `Description` - Write your reviewr's words here. 
- `Reviwer Name` - Type your reviewer name.
- `Reviwer Image` - Upload the image of reviewer.

 ![Alt Text](./assets/images/vc24.png)


### Slider Settings

[Click here](/wpbakery/#slider-settings) to see slider setting.


## Custom Box

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_custom_box.jpg)


### General
- `Style` - We have provided 2 custom banner styles. You should select any one of all.
- `Banner List` - For more banners, please click on plus icon. Once you will clickon it then more banners will show. This is how you can add multiple banners with in same shortcode.

 ![Alt Text](./assets/images/vc25.png)
- `Background Image` - Upload background image here if you want to add background picture in your custom banner.
- `Custom Title And Link` - If you want to add custom title and button to your custom banner then you can use this option.

 ![Alt Text](./assets/images/vc26.png)

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.

## Products

### Style 1 Preview
![Alt Text](./assets/images/vc104.png)


### General
- `Style` - Select style number one. 
- `Product Type Single` - Select the product type here.
- `Select Product` - This option will show only selected product at frontend.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

 ![Alt Text](./assets/images/vc29.png)

- `Style` - Select style number one. 
- `Product Type Multiple` - Select the product type here.
- `Grid View Style` - You can set number of columns with this option. 

 ![Alt Text](./assets/images/vc105.png)

- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Number` - Enter the number of products to show in this section.
- `Select Categories` - This option will show only selected categorie's post at frontend.

 ![Alt Text](./assets/images/vc106.png)

### Style 2 Preview
![Alt Text](./assets/images/vc103.png)
- `Style` - Select style number two. 
- `Select Product` - This option will show only selected product at frontend.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

### Slider Settings

[Click here](/wpbakery/#slider-settings) to see slider setting.

## Fun Fact

### Style 1 Preview
![Alt Text](./assets/images/vc108.png)
### Style 2 Preview
![Alt Text](./assets/images/vc109.png)


### General
- `Style` - You will see all styles of fun fact shortcode in general section. Select your favourite one and go to the next step.

 ![Alt Text](./assets/images/vc30.png)

### Fun Fact
- `Fun Fact Grid Type` - Select grid type to show for fact in this shortcode.
- `Add Fun Fact` - Click on **Plus Icon** to add new fun fact in your list.

 ![Alt Text](./assets/images/vc31.png)

- `Symbole` - You can add anything as symbol with this field.
- `Title` - Enter the Fact **Title** to show.
- `Fact Value` - Enter the **Fact Value** to show.

 ![Alt Text](./assets/images/vc32.png)

- `Fact Icon` - Choose the **Fact Icon** with this option. Note: icons not work on style 2.
- `Icon Color` - Choose **Icon Color**. Note: icons color not work on style 2.

 ![Alt Text](./assets/images/vc33.png)

Click on **Plus Icon** to add new fun fact.

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.

## Press Room
![Alt Text](./assets/images/vc110.png)

### Grid Style 

- `Style` - Click on first style to select **Grid Style**.
- `Grid View Listing Style` - You can show your data in 2 3 or 4 columns with this option.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Number` - Enter the number of causes to show in this section.

![Alt Text](./assets/images/vc37.png)
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place. 
- `Content Limit` - You can control description words limit with this. Enter the limit number and click on save changes.

![Alt Text](./assets/images/vc38.png)
### List Style
![Alt Text](./assets/images/vc111.png)

- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Number` - Enter the number of causes to show in this section.
- `Select Categories` - This option will show only selected categorie's post at frontend.

![Alt Text](./assets/images/vc39.png)

- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place. 
- `Content Limit` - You can control description words limit with this. Enter the limit number and click on save changes.
- `Load More` - If you want load more button then keep the button check. This button will show you at bottom of the  List style page.

![Alt Text](./assets/images/vc40.png)

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.

## Impact


### Default Style 
![Alt Text](./assets/images/vc112.png)
- `Title` - Add title of you impact here.
- `Subtitle` - Add subtitle in this field.

![Alt Text](./assets/images/vc41.png)

- `Impact List` - Click on arrow to open any impact. If you add more impacts then you should click on Plus icon and add image, name & description of impact.
- `Video Link` - If you want to include video in this section then you should put any video link in the given field.
- `Video Image` - You can upload image which will show on video. In other words, you can upload video thumbnail manually.

![Alt Text](./assets/images/vc42.png)

### Grid Style 1

![Alt Text](./assets/images/vc113.png)
Click on Plus icon to add impacts in the list. Once you added, then upload image, add title and description in the fields.

![Alt Text](./assets/images/vc43.png)

### Grid Style 2

![Alt Text](./assets/images/vc114.png)

- `Before Title` - If you want to write anything before the title then write here.
- `Title` - Add title in this field.
- `Subtitle` - Write subtitle at given place to show up or below the heading.
- `Description` - Enter descrition about impact in this field.

![Alt Text](./assets/images/vc44.png)

- `Button And Link` - Enter button text and its link.
- `Impact List` - Click on Plus icon to add impacts in the list. Once you added, then upload image, add title and description in the fields.

![Alt Text](./assets/images/vc45.png)


### Slider Settings

[Click here](/wpbakery/#slider-settings) to see slider setting.


## Volunteers

![Alt Text](./assets/images/vc115.png)

### General

- `Style` - Click to choose your favourite style.
- `Grid View` - You can select grid listing style with this option. 
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

![Alt Text](./assets/images/vc46.png)

- `Number` - Enter the number of causes to show in this section.
- `Select Categories` - This option will show only selected categorie's post at frontend.

![Alt Text](./assets/images/vc47.png)

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.

## Custom Banner

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_banner_button.jpg)


- `Style` - Click to select any style from the given list. Every style has its own field & layout styles which should be filled or selected for perfect frontend view. Like style 1 has two layout styles. So go ahead and select your favourite layout.

![Alt Text](./assets/images/vc48.png)
- `Background Image` - Upload background image here if you want to add background picture in your custom banner.
- `Title` - Add title in this field.
**Note** %STR_COLOR% used to start the theme color and %ED_COLOR% used to close the theme color.
- `Description` - Enter descrition about impact in this field.
- `Button And Link` - You can add **title & Url** of button which will show on custom banner's front view.

![Alt Text](./assets/images/vc49.png)

## Video box

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_video_box.jpg)

- `Style` - Click to select any style from the given list.
- `Video Background` - If you want to add video background image (thumbnail) manually then click ob plus icon and select image to uploade.
- `Video Link` - Add you video link here.

![Alt Text](./assets/images/vc50.png)

## Causes Grid

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_causes.jpg)

- `Style` - Click to select any style from the list. 
- `Number` - Enter the number of causes to show in this section.
- `Columns` - Choose any style from the given styles which you wnat show.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

![Alt Text](./assets/images/vc51.png)

- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place.
- `Donation` - Enable then button to show donation for causes listing
- `Button Label` - Enter button label to show in this section.

![Alt Text](./assets/images/vc52.png)

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.

## Services

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_services.jpg)

### List Style

- `Style` - Click to select any style from the list. 
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Number` - Enter the number of causes to show in this section.

![Alt Text](./assets/images/vc53.png)

- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place.
- `Content Limit` - You can control description words limit with this. Enter the limit number and click on save changes.

![Alt Text](./assets/images/vc54.png)

### Grid 1 
- `Overlap` - 
- `Border` - 
- `Style` - Click to select first style from the list. 
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

![Alt Text](./assets/images/vc55.png)

- `Number` - Enter the number of causes to show in this section.
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place.
- `Content Limit` - You can control description words limit with this. Enter the limit number and click on save changes.

![Alt Text](./assets/images/vc56.png)

### Grid 2
- `Grid View` - Click to select the number of columns.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.

![Alt Text](./assets/images/vc57.png)

- `Number` - Enter the number of causes to show in this section.
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place.

![Alt Text](./assets/images/vc58.png)

### GRid 3
- `Grid View` - Click to select the number of columns.
- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Number` - Enter the number of causes to show in this section.

![Alt Text](./assets/images/vc59.png)

- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place.
- `Content Limit` - You can control description words limit with this. Enter the limit number and click on save changes.

![Alt Text](./assets/images/vc60.png)

### Responsive Slider Setting
[Click here](/wpbakery/#responsive-slider-setting) to see responsive slider setting.


## Accordions

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_accordions.jpg)


- `Style` - Click to select any style from the given list.
- `Accordions List` - Click on arrow to open any accordion. If you add more accordions then you should click on Plus icon and select icon, write name & description in the given fields.

![Alt Text](./assets/images/vc64.png)


## Donation banner

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_donation_banner.jpg)


- `Style` - Click to select any style from the given list.
- `Subtitle` - Write subtitle at given place to show up or below the heading.
- `Title` - Add title in this field.
**Note** %STR_COLOR% used to start the theme color and %ED_COLOR% used to close the theme color.
- `Description` - Enter descrition about impact in this field.

![Alt Text](./assets/images/vc66.png)

- `Need Amount` - Type your needed amount here or how much amount do you want to collect?
- `Need Label` -  Write any name or label for the needed amount field.
- `Collect Amount` - Type the collected amount here or how much money you have been collected yet.
- `Collect Label` - Write any name or label for the collected amount field.
- `Link` - You can add **title & Url** of button which will show on donation banner.

![Alt Text](./assets/images/vc67.png)

## Donation slick slider

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_donation_slick.jpg)

###  General
- `Image` - Click on **plus icon** to upload image.
- `Subtitle` - Write subtitle at given place to show up or below the heading.
- `Title` - %STR_COLOR% used to start the theme color and %ED_COLOR% used to close the theme color.

![Alt Text](./assets/images/vc68.png)

- `Amount Label` - Enter the name or label which you want to show at targeted amount field.
- `Amount` - Type your targeted amount here.
- `Link` - You can add **title & Url** of button which will show on donation slick slider.

![Alt Text](./assets/images/vc69.png)

### Slider Settings
[Click here](/wpbakery/#slider-settings) to see slider setting.


## Fancy pop up gallery

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_fancy_pop.jpg)

You can set this shortcode in only one step. Just click on **plus icon** and select all images which you want show in as fancy gallery and click on **save changes**. 

![Alt Text](./assets/images/vc70.png)

## Gallery

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_gallery.jpg)

- `Style` - Click to select any style from the given list.
- `Number` - Enter the number of causes to show in this section.

![Alt Text](./assets/images/vc71.png)

- `Order` - You can change post's order with this option i.e Ascending order & Descending order.
- `Select Categories` - This option will show only selected categorie's post at frontend.
- `Title Limit` - You can control all post's title words count with this option. Just write the limit at given place.

![Alt Text](./assets/images/vc72.png)


## Drop Cap

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_drop_cap.jpg)

- `Style` - Click to select any style from the given list.
- `Description` - Explain your topic here and Please add %DROP%Word%CAP% for set the word into dropcap.

![Alt Text](./assets/images/vc73.png)

## Tabs

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_tabs.jpg)

- `Style` - Click to select any style from the given list.
- `Tabs List` - If you need more tabs then click on plus icon. This button will add a new tab in your tabs list.

![Alt Text](./assets/images/vc74.png)

In every tabs, we have given two fields.
- `Title` Enter the title in the given field.
- `Description` Explain you topic in description field.

![Alt Text](./assets/images/vc75.png)

## Progress Bar

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_progress_bar.jpg)

- `Style` - Click to select any style from the given list.
- `Border Radius` - Enable this button if you want border radius arround progress bar.
- `Striped` - If you want striped style(Linning) on you progress bar then enable this button.
- `Progress List` - You can add more then one progress bar with single shorcode. For multiple bars, click on plus icon and you will see one more bar added. 

![Alt Text](./assets/images/vc76.png)

In progress bar, there are many fields which should be filled.

1. Progress Color - Select the color of progress bar.
2. Progress Background Color - Select the background color of progress bar.
3. Title - Enter you progress bar title here.
4. Value - Write the value here which indicate that how much value has been completed from total.
5. Unite - Type the unit here like persentage %.

![Alt Text](./assets/images/vc77.png)

## Volunteer form

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_volunteer_form.jpg)

- `Email Template Title` - Enter form title to show in email that will be recieve to the user when you confirm volunteer.
- `Email Template Body` - Enter form body content to show in email that will be recieve to the user when you confirm volunteer.
- `Submitt Button Text` - Enter the submitt button text to show in contact us section

![Alt Text](./assets/images/vc78.png)
## Video slick slider

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_video_slick.jpg)

### General
- `Slider List` - With plus icon, you can add more slides in slide list. In every single slide you just click on plus icon and select your favourite images.
- `Title` - %STR_COLOR% used to start the theme color and %ED_COLOR% used to close the theme color.
- `Subtitle` - Write subtitle at given place to show up or below the heading.
- `Video Link` - Add your video link here.\

![Alt Text](./assets/images/vc79.png)

### Slider Settings
[Click here](/wpbakery/#slider-settings) to see slider setting.

## Contact info

### Preview
![Alt Text](https://theme-shortcodes.s3.amazonaws.com/lifeline2/lifeline2_contact_info.jpg)

- `Description` - Type your words about your contact detail.
- `Address` - Add your address in this field.
- `Info Builder` - Enable this button for custom builder.

![Alt Text](./assets/images/vc81.png)

- `Email` - Type your professional email.
- `Phone No.` - Add phone number here.
- `Website` - Write the url of your website.

![Alt Text](./assets/images/vc82.png)

